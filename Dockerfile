FROM runmymind/docker-android-sdk:latest

RUN apt-get update -qq && apt-get install -qq \
      ant lib32stdc++6 lib32z1 tar unzip && \
      useradd -m ant && chown -R ant:ant /opt/*

ENV PATH "${PATH}:/opt/android-sdk-linux/tools"
WORKDIR /home/ant/src
COPY . /home/ant/src
USER ant
